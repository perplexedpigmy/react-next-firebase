import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';
import  getAnalytics  from "firebase/analytics";

const firebaseConfig = {
  apiKey: "AIzaSyDoae3J-rLgFZKYTqMYcq1fC8Ut9dB7lSw",
  authDomain: "my-fire-next.firebaseapp.com",
  projectId: "my-fire-next",
  storageBucket: "my-fire-next.appspot.com",
  messagingSenderId: "950370110154",
  appId: "1:950370110154:web:757cc2f4f728cba6094233",
  measurementId: "G-1S461FDKTW"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} 

export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const firestore = firebase.firestore();
export const storage = firebase.storage();
export const fromMillis = firebase.firestore.Timestamp.fromMillis;
export const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp;
export const STATE_CHANGED = firebase.storage.TaskEvent.STATE_CHANGED;
export const increment = firebase.firestore.FieldValue.increment;

/**`
 * Gets a users/{uid} document with username
 * @param  {string} username
 */
export async function getUserWithUsername(username) {
  const usersRef = firestore.collection('users');
  const query = usersRef.where('username', '==', username).limit(1);
  const userDoc = (await query.get()).docs[0];
  return userDoc;
}

/**`
 * Converts a firestore document to JSON
 * @param  {DocumentSnapshot} doc
 */
export function postToJSON(doc) {
  const data = doc.data();
  return {
    ...data,
    // Gotcha! firestore timestamp NOT serializable to JSON. Must convert to milliseconds
    createdAt: data.createdAt.toMillis(),
    updatedAt: data.updatedAt.toMillis(),
  };
}
